﻿
using Microsoft.Data.SqlClient;
using PostGradManager.Models;

Console.WriteLine("Hello, World! Welcome to PG Manager");

// CRUD for managing students and professor


try
{

    using (SqlConnection connection = new SqlConnection(GetConnectionString()))
    {
        // do stuff on the database
        connection.Open();

        Student student = new Student() { FirstName = "Benita", LastName = "Smith", Age = 24 };

        string sql = "UPDATE Student SET FirstName = @FirstName, LastName = @LastName, Age = @Age WHERE ID = @ID";

        using (SqlCommand command = new SqlCommand(sql, connection))
        {
            command.Parameters.AddWithValue("@ID",4);
            command.Parameters.AddWithValue("@FirstName", student.FirstName);
            command.Parameters.AddWithValue("@LastName", student.LastName);
            command.Parameters.AddWithValue("@Age", student.Age);

            command.ExecuteNonQuery();

        }


    }

}
catch (SqlException ex)
{
    Console.WriteLine(ex.Message);
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}

void DeleteStudent(int studentID)
{

    try
    {

        using (SqlConnection connection = new SqlConnection(GetConnectionString()))
        {
            // do stuff on the database
            connection.Open();


            string sql = "DELETE FROM Student WHERE ID = @ID";

            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@ID", studentID);

                command.ExecuteNonQuery();

            }


        }

    }
    catch (SqlException ex)
    {
        Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
}

void AddStudent(Student newStudent)
{
    try
    {

        using (SqlConnection connection = new SqlConnection(GetConnectionString()))
        {
            // do stuff on the database
            connection.Open();


            string sql = "INSERT INTO Student (FirstName,LastName,Age) VALUES (@FirstName, @LastName, @Age)";

            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@FirstName", newStudent.FirstName);
                command.Parameters.AddWithValue("@LastName", newStudent.LastName);
                command.Parameters.AddWithValue("@Age", newStudent.Age);

                command.ExecuteNonQuery();

            }


        }

    }
    catch (SqlException ex)
    {
        Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
}

void DisplayStudentByID(int studentId)
{

    try
    {
        // read one student
        using (SqlConnection connection = new SqlConnection(GetConnectionString()))
        {
            // do stuff on the database
            connection.Open();


            string sql = "SELECT * FROM Student WHERE ID = @ID";

            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@ID", studentId);

                using (SqlDataReader reader = command.ExecuteReader())
                {

                    Console.WriteLine($"{reader.GetName(0)} - {reader.GetName(1)}");

                    while (reader.Read())
                    {
                        Console.WriteLine($"{reader.GetInt32(0)} - {reader.GetString(1)}");
                    }


                }


            }


        }





    }
    catch (SqlException ex)
    {
        Console.WriteLine(ex.Message);
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
}

void DisplayAllStudents()
{

    try
    {
        using (SqlConnection connection = new SqlConnection(GetConnectionString()))
        {
            // do stuff on the database
            connection.Open();

            string sql = "SELECT * FROM Student";

            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                using (SqlDataReader reader = command.ExecuteReader())
                {

                    Console.WriteLine($"{reader.GetName(0)} - {reader.GetName(1)}");

                    while (reader.Read())
                    {
                        Console.WriteLine($"{reader.GetInt32(0)} - {reader.GetString(1)}");
                    }


                }


            }


        }


    }
    catch (SqlException ex)
    {
        Console.WriteLine(ex.ToString());
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.ToString());
    }


}

string GetConnectionString()
    {
        SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder();

        connectionString.DataSource = @"NB-DEAN\SQLEXPRESS";
        connectionString.InitialCatalog = "PostGradManagerDB";
        connectionString.IntegratedSecurity = true;
        connectionString.TrustServerCertificate = true;

        return connectionString.ConnectionString;

    }